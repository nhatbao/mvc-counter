# README #

This is the setup document for MVC counter assessment.


### How to setup ###

* Download all source files from this current git repository (https://nhatbao@bitbucket.org/nhatbao/mvc-counter.git)
* Open solution file "MvcCounter.sln" via Visual Studio (recommend 2015 version)
* Right click "MvcCounter.SQLDatabase" project, then choose Publish menu item. Follow publish instructions.
* In MvcCounter project, open Web.config file, find <connectionStrings> and update SQL connection information as example. Please make sure the connection information correctly.
  
```
#!xml
<add name="Entities1" connectionString="metadata=res://*/Models.CounterModel.csdl|res://*/Models.CounterModel.ssdl|res://*/Models.CounterModel.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=(localdb)\MSSQLLocalDB;initial catalog=MvcCounter-Data;integrated security=True;multipleactiveresultsets=True;application name=EntityFramework&quot;" providerName="System.Data.EntityClient" />
```

* Right click MvcCounter project, choose Set as Startup Project
* Press F5 to run this assessment

Thanks for your reading.