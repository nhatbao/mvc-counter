﻿using MvcCounter.Models;
using MvcCounter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcCounter.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICounterService _counterService;
        private const string SES_ID = "CounterID";

        public HomeController()
        {
            _counterService = new CounterService();
        }

        public HomeController(ICounterService counterService)
        {
            _counterService = counterService;
        }

        public ActionResult Index()
        {
            Counter model;

            if (Session[SES_ID] != null)
            {
                model = _counterService.GetById((int)Session[SES_ID]);
            }
            else
            {
                model = new Counter();
                model.Id = _counterService.InitCounter();
                model.Count = 0;
                Session[SES_ID] = model.Id;
            }
            
            return View(model);
        }

        public ActionResult Hit(int id=0)
        {
            if (id <= 0)
            {
                return RedirectToAction("Index");
            }

            Counter model = new Counter();
            int result = _counterService.IncreaseCounter(id);

            if (result >= 0)
            {
                model.Id = id;
                model.Count = result;
            }

            return View("Index", model);
        }
    }
}