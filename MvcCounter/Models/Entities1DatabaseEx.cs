﻿using MvcCounter.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace MvcCounter.Models
{
    public partial class Entities1 : IDataContext
    {

        public void MarkAsModified<T>(T item) where T : class
        {
            Entry(item).State = EntityState.Modified;

        }

        public void MarkAsIgnore<T>(T item, Expression<Func<T, object>> property) where T : class
        {
            var entry = Entry(item);
            entry.Property(property).IsModified = false;

        }
    }
}