﻿using MvcCounter.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace MvcCounter.Data
{
    public interface IDataContext : IDisposable
    {
        DbSet<Counter> Counters { get; set; }

        int SaveChanges();
        Task<int> SaveChangesAsync();
        void MarkAsModified<T>(T item) where T : class;
        void MarkAsIgnore<T>(T item, Expression<Func<T, object>> property) where T : class;
    }
}