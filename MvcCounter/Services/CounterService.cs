﻿using MvcCounter.Data;
using MvcCounter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcCounter.Services
{
    public interface ICounterService
    {
        int InitCounter();
        int IncreaseCounter(int id);
        Counter GetById(int id);
    }

    public class CounterService : ICounterService
    {
        private IDataContext _dbContext;

        public CounterService()
        {
            _dbContext = new Entities1();
        }

        public CounterService(IDataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int InitCounter()
        {
            Counter countItem = new Counter();
            countItem.Count = 0;
            _dbContext.Counters.Add(countItem);
            _dbContext.SaveChanges();
            return countItem.Id;
        }

        public int IncreaseCounter(int id)
        {
            int result = -1;
            var item = _dbContext.Counters.FirstOrDefault(d => d.Id == id);

            if (item != null)
            {
                item.Count = item.Count + 1;

                if (_dbContext.SaveChanges() > 0)
                {
                    result = item.Count;
                }
            }

            return result;
        }

        public Counter GetById(int id)
        {
            return _dbContext.Counters.FirstOrDefault(d => d.Id == id);
        }
    }
}