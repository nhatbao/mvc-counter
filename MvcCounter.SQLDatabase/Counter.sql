﻿CREATE TABLE [dbo].[Counter]
(
	[Id] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [Count] INT NOT NULL
)
